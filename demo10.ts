class A {
  content = "Hi, this is class A";
  sayHello() {
    return this.content;
  }
}

class B extends A {
  sayHello() {
    // return "Hi, this is class B";
    return super.sayHello().replace("A", "B");
  }
  sayBye() {
    return "Bye";
  }
}

const a = new A();
console.log(a.sayHello());

const b = new B();
console.log(b.sayHello());
console.log(b.sayBye());
