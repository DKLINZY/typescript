// private protected public

class Person {
  protected name: string; //default as public
  public sayHello() {
    console.log(`${this.name} say hello`);
  }
}

class Teacher extends Person {
  public sayBye() {
    console.log(`${this.name} say bye`);
  }
}

const teacher = new Teacher();
teacher.sayBye();
