// class Person {
//   public name: string;
//   constructor(name: string) {
//     this.name = name;
//   }
// }

class Person {
  constructor(public name: string) {}
}

class Teacher extends Person {
  constructor(public age: number) {
    super("Shinya");
  }
}

const person = new Person("Shinya");
console.log(person.name);

const teacher = new Teacher(20);
console.log(teacher.name, teacher.age)
