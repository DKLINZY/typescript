class Person {
  constructor(private _age: number) {}
  get age() {
    return this._age - 10;
  }
  set age(age: number) {
    this._age = age + 5;
  }
}

const shinya = new Person(20);
shinya.age = 22
console.log(shinya.age);


//static

class Pet{
    // sayHello(){
    //     return 'Hello'
    // }
    static sayHello(){
        return 'Hello'
    }
}

// const dog = new Pet()
// console.log(dog.sayHello())
console.log(Pet.sayHello())