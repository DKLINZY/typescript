class Person {
  public readonly _name: string;
  constructor(name: string) {
    this._name = name;
  }
}

const person = new Person("Shinya");
// person._name = "DKLINZY";  // readonly
console.log(person._name);

//abstract

abstract class SkillSet {
  abstract skill();
}

class Waiter extends SkillSet {
  skill() {
    console.log("Place a Order");
  }
}

class Chef extends SkillSet {
  skill() {
    console.log("Prepare Ingredients");
  }
}

class SeniorChef extends SkillSet{
    skill() {
        console.log("Cooking");
      }
}
