interface A {
  bool: boolean;
  say: () => {};
}

interface B {
  bool: boolean;
  listen: () => {};
}

function whoAmi(who: A | B) {
  if (who.bool) {
    (who as B).listen();
  } else {
    (who as A).say();
  }
}

function whoAmi2(who: A | B) {
  if ("say" in who) {
    who.say();
  } else {
    who.listen();
  }
}

function add(first: string | number, second: string | number) {
  if (typeof first === "string" || typeof second === "string") {
    return `${first}${second}`;
  } else {
    return first + second;
  }
}

class NumberObj {
  count: number;
}

function addObj(first: Object | NumberObj, second: Object | NumberObj) {
  if (first instanceof NumberObj && second instanceof NumberObj) {
    return first.count + second.count;
  }
  return 0;
}
