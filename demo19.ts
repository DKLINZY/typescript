const Status = {
    ZERO: 0,
    ONE: 1,
    TWO: 2
}

enum StatusEnum{
    ZERO = 1,
    ONE,
    TWO
}

function getNumber(status: number) {
  if (status === StatusEnum.ZERO) {
    return "Zero";
  } else if (status === StatusEnum.ONE) {
    return "One";
  } else if (status === StatusEnum.TWO) {
    return "Two";
  }
}

const result = getNumber(1);
console.log(result);
