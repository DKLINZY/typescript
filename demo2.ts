let count: number = 1

// count = 'a'
// count.

interface Person {
    name: string,
    age: number
}

const peter: Person ={
    name: 'Peter',
    age: 20
}

console.log(peter.age)