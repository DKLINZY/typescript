// generics in function
function join<T>(first: T, second: T) {
  return `${first}${second}`;
}

function join2<T, P>(first: T, second: P) {
  return `${first}${second}`;
}

join<string>("hello", "typescript");
join<number>(1, 2);

join2<string, number>("string", 1);

// generics with array
// T[] or Array<T>
function genArr<T>(params: T[]) {
  return params;
}

genArr<string>(["1", "2"]);
