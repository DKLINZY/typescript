// interface Item {
//     name: string;
//   }
  
//   class SelectItem<T extends Item> {
//     constructor(private items: T[]) {}
//     getItem(index: number): string {
//       return this.items[index].name;
//     }
//   }
  
//   const selectItem = new SelectItem([
//     { name: "Item 1" },
//     { name: "Item 2" },
//     { name: "Item 3" },
//   ]);
//   console.log(selectItem.getItem(1));


class SelectItem<T extends number | string> {
  constructor(private items: T[]) {}
  getItem(index: number): T {
    return this.items[index];
  }
}

const selectItem = new SelectItem<string>(["Item 1", "Item 2", "Item 3"]);
console.log(selectItem.getItem(1));
