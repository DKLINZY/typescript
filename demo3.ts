// type

const count: number = 100
const myName: string = 'DKLINZY'
// null, undefine, boolean, void, symbol

// object
const Person: {
    name: string,
    age: number
} = {
    name: 'Peter',
    age: 20
}

// list
const People: string [] = [
    'Peter',
    'DKLINZY',
    'Shinya'
]

// class
class PersonClass{}
const peter : PersonClass = new PersonClass()

// function
const funcReturn: ()=>string = ()=>('123')