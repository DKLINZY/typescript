// type annotation
let count: number
count = 123

// type inference
let countInderence = 123


const one = 1
const two = 2
const three = one + two


function getTotal(one: number, two: number){
    return one + two
}

const total = getTotal(1, 2)

const Person = {
    name: 'Shinya',
    age: 20
}