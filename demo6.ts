// const numberArr = [1, 2, 3];
const numberArr: number[] = [1, 2, 3];
const stringArr: string[] = ["a", "b", "c"];
const undefineArr: undefined[] = [undefined, undefined];

const arr: (number | string)[] = [1, "string", 2];

// type alias

type PersonType = { name: string; age: number };
class PersonClass {
  name: string;
  age: number;
}

const obj: PersonType[] = [
  { name: "DKLINZY", age: 20 },
  { name: "Shinya", age: 22 },
];
