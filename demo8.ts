interface Candidate {
    name: string;
    mark: number;
    age?: number;
}

const candidate = {
    name: "Shinya",
    mark: 85,
    age: 24
}

const test = (candidate: Candidate)=> {
    candidate.mark >= 60 && console.log(`${candidate.name}: Pass`)
    candidate.mark < 60 && console.log(`${candidate.name}: Fail`)
}

const getDetial = (candidate: Candidate) => {
    console.log(`Name: ${candidate.name}`)
    console.log(`Mark: ${candidate.mark}`)
    candidate.age && console.log(`age: ${candidate.age}`)
}

test(candidate)
getDetial(candidate)