interface Candidate {
  name: string;
  mark: number;
  age?: number;
  [propname: string]: any; // => for other type
  say(): string;
}

interface Teacher extends Candidate {
  teach(): string;
}

class Person implements Candidate {
  name = "DKLINZY";
  age = 20;
  mark = 90;
  say() {
    return "I am DKLINZY";
  }
}

const candidate = {
  name: "Shinya",
  mark: 85,
  age: 24,
  sex: "male",
  say() {
    return "I am Shinya";
  },
};

const teacher = {
  name: "Shinya",
  mark: 85,
  age: 24,
  sex: "male",
  say() {
    return "I am Shinya";
  },
  teach() {
    return "I am a Teacher";
  },
};

const test = (candidate: Candidate) => {
  candidate.mark >= 60 && console.log(`${candidate.name}: Pass`);
  candidate.mark < 60 && console.log(`${candidate.name}: Fail`);
};

const getDetial = (candidate: Teacher) => {
  console.log(`Name: ${candidate.name}`);
  console.log(`Mark: ${candidate.mark}`);
  candidate.age && console.log(`age: ${candidate.age}`);
  candidate.sex && console.log(`sex: ${candidate.sex}`);
};

test(candidate);
getDetial(teacher);
